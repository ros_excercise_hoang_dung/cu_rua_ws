#include <ros/ros.h>
#include <turtlesim/Pose.h>
#include <iomanip> // for std::setprecision and std::fixed
#include <geometry_msgs/Twist.h>  // For geometry_msgs::Twist
#include <stdlib.h> // For rand() and RAND_MAX

// Kieu du lieu nguoi dung
struct turtle_position
{
    /* data */
    float x;
    float y;
    turtle_position (float posx, float posy)
    {
        x=posx; y=posy;
    }
};

// Danh sach bien
const float max = 2; // Kich thuwoc safe zone
const turtle_position center(5.5,5.5); // Vi tri giua man hinh
turtle_position turtle_pos = center; // Vi tri hien tai cua cu rua
turtle_position prev_turtle_pos = turtle_pos; // Vi tri truoc do cua cu rua
int count = 0; // Bien nay dem so lan cu rua khong thay doi vi tri

// Danh sach ham
bool InSafeZone(turtle_position pos, turtle_position center, float max);
void Callback(const turtlesim::Pose& msg);

// Ham main
int main(int argc, char **argv)
{
    // Bat dau node
    ros::init(argc,argv,"cu_rua_ngau_nhien");
    ros::NodeHandle nh;

    // Tao Publisher va Subscriber
    ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("turtle1/cmd_vel",1000);
    ros::Subscriber sub = nh.subscribe("turtle1/pose",1000,&Callback);
    
    // Ngau nhien
    srand(time(0));

    // Rate
    ros::Rate loop_rate(2);

    // Vong lap vo han
    while(ros::ok())
    {
        geometry_msgs::Twist pub_msg;

        ros::spinOnce();

        // Kiem tra cu rua co o vi tri cu khong
        if (turtle_pos.x == prev_turtle_pos.x && turtle_pos.y == prev_turtle_pos.y)
        {
            ++count;
        }else{
            count = 0;
        }
        if (InSafeZone(turtle_pos, center, max))
        {  
            pub_msg.linear.x = 1.0;
            // angular la so thuc trong khoang (-1.0;1.0)
            pub_msg.angular.z = 2*double(rand())/double(RAND_MAX)-1;
        }
        else if (count == 4) // Neu cu rua bi mac ket
        {
            pub_msg.linear.x = 1.0;
            pub_msg.angular.z = 5.0;
        }
        else       
        {
            // angular la so thuc trong khoang (-1.0;1.0)
            // linear la so thuc trong khoang (0.0;1.0)
            pub_msg.linear.x = double(rand())/double(RAND_MAX);
            pub_msg.angular.z = 2*double(rand())/double(RAND_MAX)-1;
        }

        prev_turtle_pos=turtle_pos;
        // Gui van toc len topic
        pub.publish(pub_msg);

        loop_rate.sleep();
    }
}

// Cac ham
bool InSafeZone(turtle_position pos, turtle_position center, float max)
{
    if (pos.x < center.x - max || pos.x > center.x + max || pos.y < center.y - max || pos.y > center.y + max) 
        return false;
    return true;
}
void Callback(const turtlesim::Pose& sub_msg)
{
    turtle_pos.x = sub_msg.x;
    turtle_pos.y = sub_msg.y;
}